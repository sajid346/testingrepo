package com.example.webtabs;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


public class CollectionPagerAdapter extends FragmentPagerAdapter {

	final int NUM_ITEMS = 3; // number of tabs

	public CollectionPagerAdapter(FragmentManager fm) {
	    super(fm);
	}

	@Override
	public Fragment getItem(int i) {
	    Fragment fragment = new TabFragment();
	    Bundle args = new Bundle();
	    args.putInt(TabFragment.ARG_OBJECT, i);
	    fragment.setArguments(args);
	    return fragment;
	}

	@Override
	public int getCount() {
	    return NUM_ITEMS;
	}

	@Override
	public CharSequence getPageTitle(int position) {
	    String tabLabel = null;
	    switch (position) {
	    case 0:
		tabLabel = "Tab 1";
		break;
	    case 1:
		tabLabel = "Tab 2";
		break;
	    case 2:
		tabLabel ="Tab 3";
		break;
	    }

	    return tabLabel;
	}
    }

    /**
     * A fragment that launches other parts of the demo application.
     */
