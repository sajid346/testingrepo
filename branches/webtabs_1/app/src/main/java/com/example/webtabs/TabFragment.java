package com.example.webtabs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

public  class TabFragment extends Fragment {

	public static final String ARG_OBJECT = "object";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {

	    Bundle args = getArguments();
	    int position = args.getInt(ARG_OBJECT);

	    int tabLayout = 0;
	    switch (position) {
	    case 0:
		tabLayout = R.layout.tab1;
		break;
	    case 1:
		tabLayout = R.layout.tab2;
		break;
	    case 2:
		tabLayout = R.layout.tab3;
		break;
	    }

	    View rootView = inflater.inflate(tabLayout, container, false);

	    return rootView;
	}
    }